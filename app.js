// Setup the modules/dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes.js")
// Server Setup

const app = express();
const port = 4001;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Add the task route
// Allows all the task routes created in the taskRoutes.js file to use / tasks route.
// parent url example localhosts:4001/tasks
app.use("/tasks", taskRoutes);

// Database Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.yy11iud.mongodb.net/B229_to-do?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

// server for mongoDB
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));


// Server Listening
app.listen(port, () => console.log(`Now listening to port ${port}.`))